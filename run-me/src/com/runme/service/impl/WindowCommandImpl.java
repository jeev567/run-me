package com.runme.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.runme.service.WindowCommand;

/**
 * @author Crunchify.com
 * 
 */

public class WindowCommandImpl implements WindowCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.runme.service.impl.WindowCommand#runCOmmand(java.lang.String)
	 */
	
	
	public WindowCommandImpl() {
	
	}
	
	@Override
	public void runCOmmand(String args) throws Exception {
		if (args == null)
			throw new Exception("No Input supplied..");
		Runtime rt = Runtime.getRuntime();
		WindowCommandImpl rte = new WindowCommandImpl();
		printOutput errorReported, outputMessage;

		try {
			//Process proc = rt.exec("cmd /c  " + args);
			Process proc = rt.exec(args);
			errorReported = rte.getStreamWrapper(proc.getErrorStream(), "ERROR");
			outputMessage = rte.getStreamWrapper(proc.getInputStream(), "OUTPUT");
			errorReported.start();
			outputMessage.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public printOutput getStreamWrapper(InputStream is, String type) {
		return new printOutput(is, type);
	}

	public class printOutput extends Thread {
		InputStream is = null;

		printOutput(InputStream is, String type) {
			this.is = is;
		}

		public void run() {
			String s = null;
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				while ((s = br.readLine()) != null) {
					System.out.println(s);
				}
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}